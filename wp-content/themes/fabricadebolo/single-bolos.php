<?php get_header(); the_post(); $term = get_the_terms($post->ID, 'bolo_category'); $banner = get_field('banner', $term[0]); //print_r($term); ?>

 <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCr2k216DPN0ARIOLQClkmXvjJgnYN8lmQ&signed_in=true&libraries=places"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/map.js"></script>
<?php /*<section class="banner" style="background-image: url(<?php echo wp_get_attachment_image_src($banner['id'],'large')[0]; ?>);">

<section class="banner" style="background-image: url(<?php echo wp_get_attachment_image_src($banner['id'],'large')[0]; ?>);">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $term[0]->name; ?></h2>
                <p><?php echo $term[0]->description; ?></p>
            </div>
        </div>
    </div>
</section> */
?>
<section id="b-<?php echo $post->ID; ?>" style="margin-top: 65px;">
        <div class="interna bolo">
            <div class="container" style="position: relative;">
                <div class="row">
                    <div class="col-sm-12">

                        <a href="<?php echo get_term_link($term[0]->term_id); ?>" class="back"><img src="<?php bloginfo('template_url'); ?>/images/icon/back.png" class="img-responsive"> Voltar para lista de bolos</a>

                        <div class="box-info">
                            <div align="center">
                                <div class="tag-bolo">
                                    <span></span>
                                    <span></span>
                                    <p class="nome_bolo"><?php the_title(); ?></p>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h2><?php echo strip_tags(get_the_content()); ?></h2>
                                </div>
                            </div>

                            <div class="row">
                              <div class="col-sm-6">

                                <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'normal')[0]; ?>" class="img">

                              </div>
                              <div class="col-sm-6">
                                  <div class="tabela-nutricional">
                                    <h2><?php echo get_field('info_nutricional');  ?></h2>
                                    <div class="table-responsive">
                                        <?php echo get_field('tabela_nutricional'); ?>
                                    </div>
                                    <p>(*) % Valores diários de referência com base em uma dieta de 2000 Kcal ou 8400 Kj. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas.</p>
                                    <p>*** VD não determinado</p>
                                  </div>
                                </div>
                            </div>

                            <div style="margin-bottom: 30px;"></div>

                            <div class="ingredientes">
                                <?php echo get_field('ingredientes');  ?>
                                
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-combo">
                                            <form action="<?php bloginfo('url'); ?>/onde-encontrar" method="get" class="form-inline text-center form-search form-map-search" role="form">
                                                <label>Procure a loja mais próxima:</label>
                                                <div class="input-group">
                                                    <input type="text" id="place_input_out" name="place" class="form-control" placeholder="Digite o endereço">
                                                   
                                                    <span class="input-group-btn">
                                                    <button type="submit" class="btn transition btn-buscar-home">Buscar</button>
                                                    <a style="margin-left: 0;" href="<?php echo bloginfo('url'); ?>/lojas">Ver todas as lojas <i class="icon icon-arrows-slim-right transition"></i></a>
                                                </span>
                                                </div>
                                            </form>
                                    </div>
                                </div>
                            </div>

                        <?php $next = get_next_post( true, '', 'bolo_category' ); $prev = get_previous_post( true, '', 'bolo_category' );  ?>
                        <?php if (!empty( $prev )): ?>
                            <a href="<?php echo get_the_permalink($prev->ID); ?>#b-<?php echo $prev->ID; ?>" class="prev"><div></div></a>
                        <?php endif; ?>
                        <?php if (!empty( $next )): ?>
                            <a href="<?php echo get_the_permalink($next->ID); ?>#b-<?php echo $next->ID; ?>" class="next"><div></div></a>
                        <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_template_part('includes/content', 'outros-bolos'); // Um coment ?>

<?php get_footer(); ?>