<?php get_header(); ?>
<?php query_posts(['post_type'=>'post','cat' => '11']); while(have_posts()): the_post();
        $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'large');
     ?>
<section class="banner" style="background-image: url(<?php echo $img[0]; ?>);">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                 <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>
<?php endwhile; wp_reset_query(); ?>
<section class="form-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-push-3">
                <div class="form-group">
                   <select name="r" class="selectpicker form-control">
                        <option selected disabled>Ordenar por</option>
                        <option value="1">De A - Z</option>
                        <option value="2">De Z a A</option>
                        <option value="3">Pelo mais antigo</option>
                        <option value="4">Pelo mais recente</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="news">
        <div class="container">
            <div class="row paginate-row">
             <?php $cont = 0; query_posts(['post_type'=>'imprensa']); while(have_posts()): the_post(); ?>
                <div class="col-sm-6">
                    <div class="box-info">
                    <?php $arq = get_field('arquivo'); ?>
                        <a target="_blank" href="<?php echo $arq['url']; ?>">
                            <div class="row">
                                <div class="col-sm-6">
                                   <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'medium', false, ['class'=>'img-responsive pull-left']); ?>
                                </div>
                                <div class="col-sm-6 box-text">
                                    <div class="text">
                                        <p><?php echo strtoupper(get_field('fonte')); ?> - <?php echo get_field('vinculacao'); ?></p>
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
             <?php 
                        $cont++; if($cont%2 == '0'): echo '</div><div class="row paginate-row">'; endif; 
                        endwhile; wp_reset_query();
                    ?>
            </div>
 
            <!--<div class="row">
                <div class="col-sm-6 col-sm-push-3">
                    <a href="#" id="paginate" data-paginate-rows="1" class="btn btn-warning btn-block btn-lg">Ver mais</a>
                </div>
            </div>-->
        </div>
    </div>
</section>
<?php get_footer(); ?>