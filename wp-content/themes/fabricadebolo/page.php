<?php get_header(); the_post(); ?>
<section>
    <div class="bg-feead1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
					<div class="panel panel-contato">
                        <div class="panel-body">
							   <?php the_content(); ?>

                 <?php if(is_page('lojas-admin')): ?>
                  <h1>Administração de lojas</h1>
                  <iframe id="iframe1" src="<?php bloginfo('url'); ?>/services/lojas_admin/public/" scrolling="no" style="width: 100%; min-height:1200px;" border="0" frameborder="0"></iframe>
                   <script type="text/javascript">
                setInterval(function(){
                    $('#iframe1').height( $('#iframe1').contents().find("body").height() );
                },500);
                </script> 
                 <?php endif; ?>
                       	</div>
           			</div>
           		</div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>