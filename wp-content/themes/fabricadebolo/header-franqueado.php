<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Fábrica de Bolo <?php wp_title(); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/gif" sizes="16x16">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.1.1/css/bootstrap-slider.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/beauty-font.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/responsive.css">
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap-slider.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider.min.js"></script>
    <?php if(is_page('onde-encontrar')): ?>

        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCr2k216DPN0ARIOLQClkmXvjJgnYN8lmQ&signed_in=true&libraries=places"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/map.js"></script>
    <?php endif; ?>
    <script src="https://ihatetomatoes.net/demos/scrolling-libraries/js/scrollReveal.js"></script>
    <script src="http://markdalgleish.com/projects/stellar.js/js/stellar.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
    <?php wp_head(); ?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="93" <?php if(is_page('onde-encontrar')): ?> class="onde-encontrar" <?php endif; ?>>
    <header>
        <nav class="navbar navbar-default navbar-white navbar-fixed-top">
            <div class="container">
            <div class="navbar-top">
                <div class="navbar-top-text">
                    <div class="navbar-top-bg transition"></div>
                    <div class="container-top">
                        <a href="http://www.franqueadovoalzira.com.br/" target="_blank">Portal do franqueado</a>
                        <a href="<?php echo home_url(); ?>"><i class="fa fa-arrow-right" aria-hidden="true"></i> voltar para o site institucional</a>
                    </div>
                </div>
                <div class="container">
                    <div class="nav-right">
                        <a href="https://www.facebook.com/fabricadebolovoalziraoficial/?fref=ts" target="_blank"><i class="icon icon-social-facebook-circle"></i></a>
                        <!-- <a href="#" target="_blank"><i class="icon icon-social-tumblr-circle"></i></a> -->
                    </div>
                </div>
            </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/header/logo-fabrica-de-bolo-vermelho.png" class="img-responsive">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- <li><a href="<?php echo bloginfo('url'); ?>">Home</a></li> -->
                        <li class="active"><a href="#seja-um-franqueado">Seja um franqueado</a></li>
                        <li><a href="#nossa-historia">Nossa História</a></li>
                        <li><a href="#vantagens">Vantagens</a></li>
                        <li><a href="#investimento">Investimento</a></li>
                        <!-- <li><a href="#">Imprensa</a></li> -->
                        <li><a href="#cadastre-se">Cadastre-se</a></li>
                    </ul>
                </div>


            </div>
        </nav>
    </header>
