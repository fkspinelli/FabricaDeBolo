<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Fábrica de Bolo <?php wp_title(); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/gif" sizes="16x16">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.1.1/css/bootstrap-slider.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/beauty-font.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/responsive.css">
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap-slider.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider.min.js"></script>
    <?php if(is_page('onde-encontrar') || is_home() || is_page('lojas')): ?>

        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCr2k216DPN0ARIOLQClkmXvjJgnYN8lmQ&signed_in=true&libraries=places&components=country:BR"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/map.js"></script>
    <?php endif; ?>
    <script src="https://ihatetomatoes.net/demos/scrolling-libraries/js/scrollReveal.js"></script>
    <script src="http://markdalgleish.com/projects/stellar.js/js/stellar.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
    <?php wp_head(); ?>
</head>

<body <?php if(is_page('onde-encontrar')): ?> class="onde-encontrar" <?php endif; ?>>

    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
            <div class="navbar-top">
                <div class="navbar-top-text">
                    <div class="navbar-top-bg transition"></div>
                    <div class="container-top">
                        <a href="http://www.franqueadovoalzira.com.br/" target="_blank">Portal do franqueado</a>
                    </div>
                </div>
                <div class="container">
                    <div class="nav-right">
                        <a href="https://www.facebook.com/fabricadebolovoalziraoficial/?fref=ts" target="_blank"><i class="icon icon-social-facebook-circle"></i></a>
                    </div>
                </div>
            </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/header/logo-fabrica-de-bolo.png" class="img-responsive">
                    </a>
                </div>

                    <div class="collapse navbar-collapse" id="navbar"><ul class="nav navbar-nav navbar-right">
                           <?php
                            wp_nav_menu( array(
                                'menu'              => 'premenu',
                                'theme_location'    => 'premenu',
                                'depth'             => 0,
                                'container'       => '',
                                'container_class' => false,
                                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                'walker'            => new wp_bootstrap_navwalker())
                            );
                        ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="javaScript:void(0);">nossos Bolos</a>
                            <ul class="dropdown-menu">
                             <?php
                                $terms = get_terms( array(
                                    'taxonomy' => 'bolo_category',
                                    'hide_empty' => false,
                                ) );
                                foreach ($terms as $t): ?>
                                <li>
                                    <a href="<?php echo esc_url( get_term_link( $t ) ) ?>">
                                         <?php $img = get_field('imagem', $t); echo wp_get_attachment_image($img['id'], 'peq', false); ?>
                                        <div>
                                            <b><?php echo $t->name; ?></b>
                                            <span><?php echo $t->description; ?></span>
                                        </div>
                                    </a>
                                </li>

                                <?php endforeach; ?>
                            </ul>
                        </li>

                        <?php
                            wp_nav_menu( array(
                                'menu'              => 'primary',
                                'theme_location'    => 'primary',
                                'depth'             => 0,
                                'container'       => '',
                                'container_class' => false,
                                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                'walker'            => new wp_bootstrap_navwalker())
                            );
                        ?>
                    </ul></div>


            </div>
        </nav>
    </header>
