<?php get_header(); $term =	$wp_query->queried_object; $banner = get_field('banner', $term); ?>
<!-- detalhe do bolo -->
<section class="banner" style="background-image: url(<?php echo wp_get_attachment_image_src($banner['id'],'large')[0]; ?>);">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $term->name; ?></h2>
                <p><?php echo $term->description; ?></p>
            </div>
        </div>
    </div>
</section>
<section class="bg-feead1">
    <div class="container">
        <div style="margin-bottom: 40px;"></div>
        
        <div class="row">  
        	<?php $i = 0; while(have_posts()): the_post(); ?> 
            <div class="col-sm-4">
                <a href="<?php the_permalink(); ?>" class="photo">
                    <div class="photo-bg" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail')[0]; ?>);">
                        <div class="filter"></div>
                    </div>
                    <div class="content">
                        <p><?php the_title(); ?></p>
                    </div>
                </a>
            </div>
            <?php $i++; if($i%3 == 0): '</div><div class="row">'; endif; ?>
          	<?php endwhile; ?>
        </div>
       


        <div style="margin-bottom: 70px;"></div>
    </div>
</section>

<?php get_footer(); ?>