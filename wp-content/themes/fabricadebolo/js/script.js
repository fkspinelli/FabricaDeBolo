$(document).ready(function(){
  $(window).on('load resize', function(){
   // $('.navbar-top-text').css({'left':$('.navbar-right').offset().left-10});
    $('#map').parents('.row').find('[class*="col-"]').height($(window).height() - $('header nav').height() - $('.search').height() - 40);
    $('#banner .carousel-inner>.item').css({'height':$(window).height()});
    if($(window).width() > 768){
      $('.onde-encontrar .col-list-map ul').height($(window).height()-$('.navbar').height()-$('.search').height()-$('.onde-encontrar .col-list-map>.panel').height()-$('footer').height() + 75);
    }else{
      $('.onde-encontrar .col-list-map ul, .col-list-map').css({'height':'auto'});
    }
  });

  var _maxSlides = 3;
  var _slideWidth = 300;

  if($(window).width() < 992 && $(window).width() > 769){
    _slideWidth = 220;
  }else if($(window).width() < 768 && $(window).width() > 541){
    _maxSlides = 2;
    _slideWidth = 240;
  }else if($(window).width() < 540){
    _maxSlides = 1;
    _slideWidth = 245;
  }

  slider = $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: _maxSlides,
        slideWidth: _slideWidth,
        slideMargin: 30,
        responsive: true
    });


  $("#radio").slider();

  $('.col-list-map ul').on('load scroll', function(){
    if($(this).scrollTop() > 0){
      $(this).parent('.list-shadow').find('.list-shadow.top').addClass('active');
    }else{
      $(this).parent('.list-shadow').find('.list-shadow.top').removeClass('active');
    }

        if($(this).scrollTop() + $(this).innerHeight() < $(this)[0].scrollHeight) {
            $(this).parent('.list-shadow').find('.list-shadow.bottom').addClass('active');
        }else{
          $(this).parent('.list-shadow').find('.list-shadow.bottom').removeClass('active');
        }

  });

  if($(window).width() > 768){
    //scrollReveal
    // window.scrollReveal = new scrollReveal({ reset: false });
  }

    $("#banner").on('slide.bs.carousel', function (e) {
        $(".active", e.target).find('.carousel-text h2').animate({'opacity':'0'}, function(){
          $(this).animate({'margin-top':'10px'});
        });

        $(".active", e.target).find('.carousel-text p').animate({'opacity':'0'}, function(){
          $(this).animate({'margin-top':'10px'});
        });
    });

    $("#banner").on('slid.bs.carousel', function (e) {
      $(".active", e.target).find('.carousel-text h2').animate({'margin-top':'0', 'opacity':'1'});
      $(".active", e.target).find('.carousel-text p').animate({'margin-top':'0', 'opacity':'1'});
    });

  //   $('.paginate-row').addClass('hide');
  //   paginate();
  //   $('#paginate').click(function(event){
  //    event.preventDefault();
  //    paginate();
  //   });

  //   function paginate(){
  //    var paginateRows = parseInt($('#paginate').attr('data-paginate-rows'));
    // $('.paginate-row').each(function(){
    //  var index = $(this).index()+1;

    //  if($(this).find('*').size() != 0){
    //    if((index) > paginateRows){
    //      return false;
    //    }
    //    alert(index);
    //    $(this).show().removeClass('hide');
    //  }
    // });
  //   }


        $('.navbar-white.navbar-default .navbar-nav>li>a').click(function() {
          var href = $(this).attr('href');
            $('html, body').animate({
              scrollTop: $(href).offset().top - $('.navbar-white').height() + 1
            }, 1000);
        });

});
