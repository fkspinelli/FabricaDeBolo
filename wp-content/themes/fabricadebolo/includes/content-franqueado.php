<!-- SEJA FRANQUEADO -->
    <div class="home-seja-um-franqueado">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <img src="<?php bloginfo('template_url'); ?>/images/tag/seja-um-franqueado.png" class="img-responsive tag" data-scroll-reveal="enter bottom and move 20px over 1s">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <img src="<?php bloginfo('template_url'); ?>/images/seja-um-franqueado/bolo-barato.png" class="img-responsive center item left" data-scroll-reveal="enter left and move 40px over 1s">
                    <img src="<?php bloginfo('template_url'); ?>/images/seja-um-franqueado/operacoes-simples.png" class="img-responsive center item left" data-scroll-reveal="enter left and move 40px over 1s">
                </div>
                <div class="col-sm-4">
                    <img src="<?php bloginfo('template_url'); ?>/images/seja-um-franqueado/vo-alzira.png" class="img-responsive center vo-alzira" data-scroll-reveal="enter bottom and move 40px over 1s">
                </div>
                <div class="col-sm-4">
                    <img src="<?php bloginfo('template_url'); ?>/images/seja-um-franqueado/nao-cobramos-royalties.png" class="img-responsive center item right" data-scroll-reveal="enter right and move 40px over 1s">
                    <img src="<?php bloginfo('template_url'); ?>/images/seja-um-franqueado/mais-de-150-lojas.png" class="img-responsive center item right" data-scroll-reveal="enter right and move 40px over 1s">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <a href="<?php bloginfo('url'); ?>/seja-um-franqueado/" class="btn btn-danger text-uppercase transition" data-scroll-reveal="enter">saiba mais</a>
                </div>
            </div>
        </div>
    </div>
    <!--/ SEJA FRANQUEADO -->