<!-- BANNER HOME -->
<?php $args = array('post_type'=>'banners'); query_posts($args); ?>
<div id="banner" class="carousel slide" data-ride="carousel">
    <div class="carousel-indicators-container">
        <div class="container">
            <ol class="carousel-indicators">
                <?php $cont = -1; while(have_posts()): the_post(); $cont++; ?>
                <li data-target="#banner" data-slide-to="<?php echo $cont; ?>" class="<?php echo $cont == '0' ? 'active' : null; ?>"></li>
                <?php endwhile; ?>
            </ol>
        </div>
    </div>
    <div class="carousel-inner" role="listbox">
        <?php $cont_w = -1; while(have_posts()): the_post(); $cont_w++; $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'large'); ?>
        <div class="item <?php echo $cont_w == '0' ? 'active' : null; ?>" style="background-image: url(<?php echo $img[0]; ?>);">
         <a style="text-decoration: none;" href="<?php echo get_field('link_externo'); ?>">
            <div class="carousel-text">
                <h2><?php the_title(); ?></h2>
                <?php the_excerpt(); ?>
            </div>
            </a>
        </div>
        <?php endwhile; wp_reset_query(); ?>
    </div>
    <a class="left carousel-control" href="#banner" role="button" data-slide="prev">
        <img src="<?php bloginfo('template_url'); ?>/images/banner/seta-esquerda.png">
    </a>
    <a class="right carousel-control" href="#banner" role="button" data-slide="next">
        <img src="<?php bloginfo('template_url'); ?>/images/banner/seta-direita.png">
    </a>

    <?php get_template_part('includes/content','onde-encontrar'); // FORM DE ONDE ENCONTRAR ?>

</div>
<!--/ BANNER HOME -->