<?php $args = array('post_type'=>'bolos', 'post__not_in'=>array($post->ID)); query_posts($args); if(have_posts()): // Outros bolos ?>
<section>
    <div class="outros-bolos">
        <div class="container">
            <img src="<?php bloginfo('template_url'); ?>/images/tag/outros-bolos.png" class="img-responsive tag">

         
            <div class="box-outros-bolos">
                <ul class="bxslider">
                    <?php while(have_posts()): the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>" class="photo">
                            <div class="photo-bg" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail')[0]; ?>);">
                                <div class="filter"></div>
                            </div>
                            <div class="content">
                                <p><?php the_title(); ?></p>
                            </div>
                        </a>                           
                    </li>
                   <?php endwhile; wp_reset_query(); ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php endif; // Fim de Outros Bolos ?>