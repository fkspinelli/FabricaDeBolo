<div class="carousel-search">
    <div class="container">
       
        <form action="<?php bloginfo('url'); ?>/onde-encontrar" method="get" class="form-inline text-center form-search form-map-search" role="form">
            <label data-scroll-reveal="enter left and move 20px over 1s" data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">Procure a loja mais próxima:</label>
            <div class="input-group" data-scroll-reveal="enter right and move 20px over 1s" data-scroll-reveal-id="2" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                <input type="text" id="place_input_out" name="place" class="form-control" placeholder="Digite o endereço">
              
                <span class="input-group-btn">
                <button type="submit" class="btn btn-buscar-home transition">Buscar</button>
            </span>
            </div>
        </form>
        <a href="<?php echo bloginfo('url'); ?>/lojas">Ver todas as lojas <i class="icon icon-arrows-slim-right transition"></i></a>
    </div>
</div>