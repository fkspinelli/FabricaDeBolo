<?php 
include_once("./../../../../wp-config.php"); 
require_once('Geocoder/Geocoder.php');


global $wpdb;

// Gero o Webservice, realizando consulta aos franquados
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://www.franqueadovoalzira.com.br/api/franquia');
curl_setopt($curl, CURLOPT_HTTPHEADER, array('aceppt: application/json', 'token: voalzira'));
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$resp = curl_exec($curl);

$rows = json_decode($resp);

curl_close($curl);

echo "<pre>";
print_r($rows);
echo "</pre>";

/*
foreach ($rows as $k => $loja):
	//$lat = $lng = '';
	// Pegando primeiro a lat/lng que vem do webservice
	 if(!is_null($loja->Latitude)){
	    $lat = $loja->Latitude;
	    $lng = $loja->Longitude;
	  }else{
	    // realizo o chamamento na classe Geocoder
	    $formatted_address = $loja->Endereco.' '.$loja->Numero.' '.$loja->Localidade.'  '.$loja->UF;
	    $geocoder = new GoogleMapsGeocoder($formatted_address);
	    $geo = $geocoder->geocode();

	    $lat = $geo['results'][0]['geometry']['location']['lat'];
	    $lng = $geo['results'][0]['geometry']['location']['lng'];
	  }
	
	$wpdb->replace( 
	'tb_lojas', 
		array( 
	        'id' => $loja->ID,
			'nome' => $loja->Nome, 
			'razao_social' => $loja->RazaoSocial,
			'cnpj' => $loja->CNPJ,
			'tel' => $loja->Telefone,
			'numero' => $loja->Numero,
			'complemento' => $loja->Complemento,
			'data_cadastro' => $loja->DataCadastro,
			'data_fundacao' => $loja->DataFundacao,
			'data_atualizacao' => date('Y-m-d H:i:s'),
			'email' => $loja->Email[0],
			'uf' => $loja->UF,
			'cidade' => $loja->Localidade,
			'bairro' => $loja->Bairro,
			'endereco' => $loja->Endereco,
			'horarios' => serialize($loja->horarios),
			'horario_especial' => serialize($loja->horarioEspecial),
			'latitude' => $lat,
			'longitude' => $lng,

		), 
		array( 
	        '%d',
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
			'%s', 
		) 
	);

echo $wpdb->print_error();
endforeach;*/