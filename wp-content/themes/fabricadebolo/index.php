<?php get_header(); ?>
    <section>

        <?php get_template_part('includes/content', 'banner'); // BANNER HOME ?>

        <!-- TIPOS DE BOLO -->
        <div class="home-nossos-bolos">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <img src="<?php bloginfo('template_url'); ?>/images/tag/nossos-bolos.png" class="img-responsive tag" data-scroll-reveal="enter bottom and move 20px over 1s">
                    </div>
                </div>
                <div class="row">
                    <?php
                    $terms = get_terms( array(
                        'taxonomy' => 'bolo_category',
                        'hide_empty' => false,
                        'number' => 4
                    ) );
                    foreach ($terms as $t): ?>
                    <div class="col-sm-6">
                        <a href="<?php echo esc_url( get_term_link( $t ) ) ?>">
                            <div class="box-bolo" data-scroll-reveal="enter bottom and move 20px over 1s">
                              
                                <?php $img = get_field('imagem', $t); echo wp_get_attachment_image($img['id'], 'large', false, ['class'=>'img-responsive']); ?>
                               
                                <div class="box-text">
                                    <div class="box-text-content">
                                        <div>
                                            <h3><?php echo $t->name; ?></h3>
                                            <p><?php echo $t->description; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div> 
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <!--/ TIPOS DE BOLO -->

        <!-- NOVIDADES -->
<!--         <div class="home-novidades">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <img src="<?php bloginfo('template_url'); ?>/images/tag/novidades.png" class="img-responsive tag" data-scroll-reveal="enter bottom and move 20px over 1s">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="carouselNovidades" class="carousel slide" data-ride="carousel" data-scroll-reveal="enter bottom and move 20px over 1s">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselNovidades" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselNovidades" data-slide-to="1"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                          
                                <div class="active item">
                                    <div class="row">
                                    <?php $i = 0; while(have_posts()): the_post();  ?>
                                        <div class="col-sm-4">
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="row">
                                                    <div class="col-sm-6"> 
                                                    <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'thumbnail', false, ['class'=>'img-responsive pull-left']); ?>
                                                   </div>
                                                    <div class="col-sm-6">
                                                        <div class="text">
                                                            <h4><?php the_title(); ?></h4>
                                                            <?php the_excerpt(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php 

                                             $i++;
                                        if($i%3 == 0): echo '</div></div><div class="item"><div class="row">'; endif;

                                        endwhile; wp_reset_query(); ?>
                                        
                                  </div>

                                  </div>
                                     
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--/ NOVIDADES -->

       <?php get_template_part('includes/content', 'franqueado'); //SEJA UM FRANQUEADO ?>

    </section>
 <?php get_footer(); ?>