<?php
require_once('resources/menu-walker.php');


// Criando os Hooks
add_action('add_lojas_registros','add_lojas_registros_func');
add_action('del_lojas_registros','del_lojas_registros_func');
function del_lojas_registros_func()
{

global $wpdb;
    $lojas = $wpdb->get_results( 
    "
    SELECT * 
    FROM tb_lojas
    "
  );

  $ids = $news = array();

  foreach ($lojas as $result) {

    $wpdb->delete( 'tb_lojas', array( 'id' => $result->id ) );
  }
}
function add_lojas_registros_func()
{
  require_once('resources/Geocoder/Geocoder.php');
  
global $wpdb;
  // Gero o Webservice, realizando consulta aos franquados
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://www.franqueadovoalzira.com.br/api/franquia');
curl_setopt($curl, CURLOPT_HTTPHEADER, array('aceppt: application/json', 'token: voalzira'));
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$resp = curl_exec($curl);

$rows = json_decode($resp);

curl_close($curl);

foreach ($rows as $loja):

 $lat = $lng = '';
  // Pegando primeiro a lat/lng que vem do webservice
  if(!is_null($loja->Latitude)){
    $lat = $loja->Latitude;
    $lng = $loja->Longitude;
  }else{
    // realizo o chamamento na classe Geocoder
    $formatted_address = $loja->Endereco.' '.$loja->Numero.' '.$loja->Localidade.'  '.$loja->UF;
    $geocoder = new GoogleMapsGeocoder($formatted_address);
    $geo = $geocoder->geocode();

    $lat = $geo['results'][0]['geometry']['location']['lat'];
    $lng = $geo['results'][0]['geometry']['location']['lng'];
  }

  
  
  $wpdb->replace( 
  'tb_lojas', 
    array( 
      'id' => $loja->ID,
      'nome' => $loja->Nome, 
      'razao_social' => $loja->RazaoSocial,
      'cnpj' => $loja->CNPJ,
      'tel' => $loja->Telefone,
      'numero' => $loja->Numero,
      'complemento' => $loja->Complemento,
      'data_cadastro' => $loja->DataCadastro,
      'data_fundacao' => $loja->DataFundacao,
      'data_atualizacao' => date('Y-m-d H:i:s'),
      'email' => $loja->Email[0],
      'uf' => $loja->UF,
      'cidade' => $loja->Localidade,
      'bairro' => $loja->Bairro,
      'endereco' => $loja->Endereco,
      'numero' => $loja->Numero,
      'complemento' => $loja->Complemento,
      'horarios' => serialize($loja->horarios),
      'horario_especial' => serialize($loja->horarioEspecial),
      'latitude' => $lat,
      'longitude' => $lng,

    ), 
    array( 
          '%d',
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
      '%s', 
    ) 
  );

echo $wpdb->print_error();
endforeach;
}


add_image_size('peq', 95,57,true);

// Adicionando excerpt para paginas
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}


add_action( 'pre_get_posts', function ( $query ) 
{
    if (    !is_admin() 
         && $query->is_main_query() 
         && $query->is_tax() 
    ) {
        $query->set( 'posts_per_page', '999'   );
        //$query->set( 'orderby',        'rand' );
    }
});

// Menus
register_nav_menus( array(
    'primary' => __( 'Menu Principal', 'fabricadebolo' ),
    'premenu' => __( 'Menu Predecessor', 'fabricadebolo' ),
) );
function remove_ul ( $menu ){
    return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}
add_filter( 'wp_nav_menu', 'remove_ul' );

function custom_remove_cpt_slug( $post_link, $post, $leavename ) {

    if ( 'bolos' != $post->post_type || 'publish' != $post->post_status ) {
      return $post_link;
    }
    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    return $post_link;
}

add_filter( 'post_type_link', 'custom_remove_cpt_slug', 10, 3 );


// Removendo pot type da url
function fb_parse_request_trick( $query ) {
 
    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;
 
    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }
 
    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'page', 'bolos' ) );
    }
}
add_action( 'pre_get_posts', 'fb_parse_request_trick' );


the_posts_pagination( array(
    'mid_size'  => 2,
    'prev_text' => __( 'Back', 'textdomain' ),
    'next_text' => __( 'Onward', 'textdomain' ),
) );

if (function_exists('register_sidebar')) {
    register_sidebar(array(
		'name'=>'Sidebar',
    'before_widget'=>'<div class="widget">',
    'after_widget'=>'</div>',
		'before_title'=>'<h3>',
		'after_title'=>'</h3>',
    ));
}

function getPercent($v, $t){
    $res = ($v*100)/$t;
    return round($res);
}
function getVideoID($video_src){
       $video_id = false;
    $url = parse_url($url);
    if (strcasecmp($url['host'], 'youtu.be') === 0)
    {
        #### (dontcare)://youtu.be/<video id>
        $video_id = substr($url['path'], 1);
    }
    elseif (strcasecmp($url['host'], 'www.youtube.com') === 0)
    {
        if (isset($url['query']))
        {
            parse_str($url['query'], $url['query']);
            if (isset($url['query']['v']))
            {
                #### (dontcare)://www.youtube.com/(dontcare)?v=<video id>
                $video_id = $url['query']['v'];
            }
        }
        if ($video_id == false)
        {
            $url['path'] = explode('/', substr($url['path'], 1));
            if (in_array($url['path'][0], array('e', 'embed', 'v')))
            {
                #### (dontcare)://www.youtube.com/(whitelist)/<video id>
                $video_id = $url['path'][1];
            }
        }
    }
    return $video_id;
}


add_theme_support( 'post-thumbnails' );

add_action( 'init', 'create_post_type' );

function create_post_type() {
  register_post_type( 'banners',
    array(
      'labels' => array(
        'name' => __( 'Banners' ),
        'singular_name' => __( 'Banner' )
      ),
      'menu_position'         => 4,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','excerpt','thumbnail')
    )
  );
  register_post_type( 'bolos',
    array(
      'labels' => array(
        'name' => __( 'Bolos' ),
        'singular_name' => __( 'Bolo' ),
        'add_new_item'  => __( 'Adicionar novo bolo'),
      ),
      'public' => true,
      'has_archive' => true,
      'taxonomies'            => array( 'bolo_category' ),
      'menu_position'         => 5,
      'rewrite' => array('slug' => 'bolos'),
      'supports' => array('title','thumbnail','excerpt','editor','revisions')
    )
  );

  /*register_post_type( 'lojas',
    array(
      'labels' => array(
        'name' => __( 'Lojas' ),
        'singular_name' => __( 'Loja' ),
        'add_new_item'  => __( 'Adicionar nova loja'),
      ),
      'public' => true,
      'has_archive' => true,
     
      'menu_position'         => 5,
      'rewrite' => array('slug' => 'lojass'),
      'supports' => array('title','thumbnail','excerpt','editor','revisions')
    )
  );*/

  register_post_type( 'imprensa',
    array(
      'labels' => array(
        'name' => __( 'Imprensa' ),
        'singular_name' => __( 'Imprensa' ),
        'add_new_item'  => __( 'Adicionar novo clipping'),
      ),
      'public' => true,
      'has_archive' => true,
      //'taxonomies'            => array( 'bolo_category' ),
      'menu_position'         => 5,
      'rewrite' => array('slug' => 'imprensa'),
      'supports' => array('title','thumbnail','excerpt','editor','revisions')
    )
  );
  register_post_type( 'testemunhos',
    array(
      'labels' => array(
        'name' => __( 'Testemunhos' ),
        'singular_name' => __( 'Testemunho' ),
        'add_new_item'  => __( 'Adicionar novo testemunho'),
      ),
      'public' => true,
      'has_archive' => true,
      //'taxonomies'            => array( 'bolo_category' ),
      'menu_position'         => 5,
      'rewrite' => array('slug' => 'testemunho'),
      'supports' => array('title','thumbnail','editor','revisions')
    )
  );

}

//Quando der problema no post type acionar esta função
//flush_rewrite_rules();


function add_query_vars_filter( $vars ){
  $vars[] = "c";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

// Adicionando Taxonomias
add_action( 'init', 'custom_taxonomy', 0 );

function custom_taxonomy() {

    $labels_bolo_category = array(
    'name'                       => _x( 'Tipo de bolos', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Tipo de bolos', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Tipo de bolo', 'text_domain' )
    );
  

// Categoria de bolos
   $args_bolos = array(
    'labels'                     => $labels_bolo_category,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    'rewrite'                    => array('slug' => 'nossos-bolos')
   
  );

  register_taxonomy( 'bolo_category', array( 'bolos' ), $args_bolos );

 

}

add_action('admin_menu', 'example_admin_menu');
 
/**
* add external link to Tools area
*/
function example_admin_menu() {
    global $submenu;
    $url = get_bloginfo('url').'/lojas-admin/';
    $submenu['index.php'][] = array('Admin Lojas', 'manage_options', $url);
}

/**
  *Registrando Widget
**/

add_action( 'widgets_init', 'eletros_widgets_init' );
function eletros_widgets_init() {
   
    # Registradno Sidebars
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'eletros-main-sidebar' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets nesta área serão mostradas na barra lateral', 'eletros-main-sidebar' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
      ) );
}

// Pegando a categoria principal
function getMainCategory($post_id, $tax){

  $post_categories = get_the_terms($post_id, $tax);
  $cats = array();                    
  foreach($post_categories as $c){
      $cat = get_category( $c );
     
      if(($cat->slug != 'sem-categoria') && $cat->slug != 'destaque'){
          $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug, 'id' => $cat->term_id );
      }
  }
  return $cats;
}

// Breadcrumbs
function eletros_breadcrumbs() {
       
    // Settings
    $separator          = '';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Página inicial';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo ' <div class="breadcrumb"><div class="container"><i class="icon icon-circle-slelected"></i><ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul></div></div>';
           
    }
       
}

// Criando uma action para performar o WebService
function fabricadebolo_lojas_hook()
{
    global $wpdb;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://www.franqueadovoalzira.com.br/api/franquia');
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('aceppt: application/json', 'token: voalzira'));
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $resp = curl_exec($curl);

    $rows = json_decode($resp);

    curl_close($curl);

    foreach ($rows as $loja):
    $wpdb->replace( 
    'tb_lojas', 
        array( 
            'id' => $loja->ID,
            'nome' => $loja->Nome, 
            'razao_social' => $loja->RazaoSocial,
            'cnpj' => $loja->CNPJ,
            'data_cadastro' => $loja->DataCadastro,
            'data_fundacao' => $loja->DataFundacao,
            'data_atualizacao' => date('Y-m-d H:i:s'),
            'cep' => $loja->cep,

        ), 
        array( 
            '%d',
            '%s', 
            '%s', 
            '%s', 
            '%s', 
            '%s', 
            '%s', 
            '%s', 
        ) 
    );
    if(!$wpdb->print_error()){
        return true;
    }

    echo $wpdb->print_error();
    return false;

    endforeach;
}
do_action( 'fabricadebolo_lojas_hook', 'fabricadebolo_lojas_hook' );

/*// Register Custom Post Type
function custom_post_type() {

  $labels = array(
    'name'                  => _x( 'Post Types', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Post Type', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Post Types', 'text_domain' ),
    'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add New', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Post Type', 'text_domain' ),
    'description'           => __( 'Post Type Description', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,    
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'post_type', $args );

}
add_action( 'init', 'custom_post_type', 0 );



// Register Custom Taxonomy
function custom_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Taxonomies', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Taxonomy', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Taxonomy', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'taxonomy', array( 'post' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );




*/

?>