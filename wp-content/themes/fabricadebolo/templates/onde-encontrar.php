<?php 
/*
* Template Name: Onde Encontrar
*/
get_header();  ?>
 <section>
        <div class="search">
            <div class="container">
                <form action="<?php bloginfo('url'); ?>/onde-encontrar" method="get" class="form-inline text-center form-search form-place-search" role="form">
                    <label>Procure a loja mais próxima:</label>
                    <div class="input-group">
                        <input type="text" value="<?php echo isset($_GET['place']) ? $_GET['place'] : null; ?>" id="place_input" name="place" class="form-control" placeholder="Digite o endereço">
                        <input type="hidden" name="lat">
                        <input type="hidden" name="lon">
                        <span class="input-group-btn">
                        <button class="btn btn-buscar transition" type="button">Buscar</button>
                    </span>
                    </div>
                </form>
                <a href="<?php echo bloginfo('url'); ?>/lojas">Ver todas as lojas <i class="icon icon-arrows-slim-right transition"></i></a>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-list-map">
                    <!-- <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="raio">Zoom: <b>+<span>1</span>km</b></p>
                            <input 
                                id="radio"
                                data-slider-id='radioSlider'
                                type="text" 
                                name="somename" 
                                data-provide="slider" 
                                data-slider-min="5" 
                                data-slider-max="9"
                                data-slider-step="1"
                                data-slider-value="5"
                                data-slider-tooltip="hide"
                             style="width:100%;">
                        </div>
                    </div> -->
                    <div class="list-shadow">
                        <div class="list-shadow top"></div>
                        <ul>

                        </ul>
                        <div class="list-shadow bottom active"></div>
                    </div>
                </div>
                <div class="col-sm-6 col-map">
                    <div style="width: 100%; height: 100%;" id="map"></div>
                </div>
            </div>
        </div>
    </section>
<script>

 function codeAddress(place, map) {
    var address = place;
     geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
       /* var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
        });*/
      } else {
        alert("O endereço procurado não foi encontrado, por favor, faça uma nova busca mais específica.");
       
      }
    });
}
   function initMap() {
     var geocoder;
    
     
    var center = { lat: -23.0129191, lng: -43.3073804 };
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false,
        rotateControl: false
    });
    <?php if(isset($_GET['place'])): ?> 

        codeAddress("<?php echo $_GET['place']; ?>", map);
    
    <?php endif; ?>
        
  $('.btn-buscar').click(function(event){
    var options = {

        componentRestrictions: {country: "br"}
       };
      var input = (document.getElementById('place_input'));
      var autocomplete = new google.maps.places.Autocomplete(input, options);
      listenPlace(autocomplete);
      event.preventDefault();
      return;
  });
  $('#place_input').keydown(function(event){   
    var options = {

        componentRestrictions: {country: "br"}
       };
    var input = (document.getElementById('place_input'));
    var autocomplete = new google.maps.places.Autocomplete(input, options); 
    /* listenPlace(autocomplete);
      getData();
      if(event.keyCode==13){
        event.preventDefault();
         return;
      }*/

  });
    

    var myCity = new google.maps.Circle({
      center:center,
      radius:2500,
      strokeColor:"#0000FF",
      strokeOpacity:0,
      strokeWeight:2,
      fillColor:"#0000FF",
      fillOpacity:0
    });


    $("#radio").on("change", function(slideEvt) {
        var km = 5;
        switch (slideEvt.value.newValue) {
            case 5:
                km = 1;
                break;
            case 6:
                km = 5;
                break;
            case 7:
                km = 15;
                break;
            case 8:
                km = 30;
                break;
            case 9:
                km = 55;
                break;
        }
        $('b span').text(km);
        myCity.set('radius', km*1000);
        map.fitBounds(myCity.getBounds());
    });


    myCity.setMap(map);
    
   

    window.resize;

    // Faz algo quando a janela de exibição do mapa é alterada
    map.addListener('idle', function() {
       
         getData(); 
    });


    map.fitBounds(myCity.getBounds());




}
google.maps.event.addDomListener(window, 'load', initMap);

function listenPlace(autocomplete)
{
    autocomplete.addListener('place_changed', function(event) {
    
    var place = autocomplete.getPlace();
    
    if (!place.geometry) {
      //window.alert("Autocomplete's returned place contains no geometry");
      event.preventDefault();
      return;
    }
        
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: place.geometry.location,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        });
        getData(); 
    });  

}

function getData() {
   
    var currentBounds = map.getBounds();
    
    var NewMapCenter = map.getCenter();
    //will give you LatLng object, so you can call

    var lat = NewMapCenter.lat();
    var lon = NewMapCenter.lng();

    console.log(lat+" - "+lon);


    // Faz requisição ao servidor pedindo os dados
    $.ajax({
        url: "/wp-content/themes/fabricadebolo/resources/onde-encontrar.php",
        type: "POST",
        dataType: 'json',
        data: {'currentBounds': JSON.stringify(currentBounds), lat: lat, lon: lon},
        error: function(err){
            console.log(err);
        }
    }).done(function(result){
       //console.log(result[0]);
           var h = '';
            for (i = 0; i < result[0].length; i++) {
                if(result[0][i].latitude != null && result[0][i].longitude != null){
                   
                    var latlng = new google.maps.LatLng(result[0][i].latitude, result[0][i].longitude);
                    var lojaId = result[0][i].id;
                    var loja = result[0][i].nome;
                    var endereco = result[0][i].endereco;
                    var telefone = result[0][i].tel;
                    var num = result[0][i].numero;
                    var comp = result[0][i].complemento;
                    var horario = result[1][i];
                    var horario_especial = result[2][i];
                    
                  
                    // insere novo marcador se esta na area visive do mapa e ainda não foi inserido
                    if (currentBounds.contains(latlng) && isLocationFree([result[0][i].latitude, result[0][i].longitude])) {
                        lookup.push([result[0][i].latitude, result[0][i].longitude]);
                        addMarker(latlng, lojaId, loja, endereco, num, comp, telefone, horario, horario_especial);
                    }
                }

                h = '';
            }
            showInfo();
    });
}

function addMarker(latlng, lojaId, loja, endereco, num, comp, telefone, horario, horario_especial) {

    // pin customizado
    var icon = {
        path: "M322.621,42.825C294.073,14.272,259.619,0,219.268,0c-40.353,0-74.803,14.275-103.353,42.825 c-28.549,28.549-42.825,63-42.825,103.353c0,20.749,3.14,37.782,9.419,51.106l104.21,220.986 c2.856,6.276,7.283,11.225,13.278,14.838c5.996,3.617,12.419,5.428,19.273,5.428c6.852,0,13.278-1.811,19.273-5.428 c5.996-3.613,10.513-8.562,13.559-14.838l103.918-220.986c6.282-13.324,9.424-30.358,9.424-51.106 C365.449,105.825,351.176,71.378,322.621,42.825z M270.942,197.855c-14.273,14.272-31.497,21.411-51.674,21.411 s-37.401-7.139-51.678-21.411c-14.275-14.277-21.414-31.501-21.414-51.678c0-20.175,7.139-37.402,21.414-51.675 c14.277-14.275,31.504-21.414,51.678-21.414c20.177,0,37.401,7.139,51.674,21.414c14.274,14.272,21.413,31.5,21.413,51.675 C292.355,166.352,285.217,183.575,270.942,197.855z",
        fillColor: '#ff0000',
        fillOpacity: 1,
        anchor: new google.maps.Point(0, 0),
        strokeWeight: 1,
        scale: 0.1
    }

    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        lojaId: lojaId,
        loja: loja,
        endereco: endereco,
        num: num,
        comp: comp,
        telefone: telefone,
        horario: horario,
        horario_especial: horario_especial,
        icon: icon1
    });
    markers.push(marker);

    google.maps.event.addListener(marker, 'mouseover', function() {
        marker.setIcon(icon2);
        $('#loja_'+marker.lojaId).addClass('active');
    });
    google.maps.event.addListener(marker, 'mouseout', function() {
        marker.setIcon(icon1);
        $('#loja_'+marker.lojaId).removeClass('active');
    });

    google.maps.event.addListener(marker, 'click', function(e) {
        $('.onde-encontrar .col-list-map ul').animate({
            scrollTop: $('#loja_'+marker.lojaId).attr('data-top')
        });

        // alert( typeof $('#loja_'+marker.lojaId).attr('data-top') );
    });


}

$(document).on('mouseover mouseout', '.onde-encontrar .col-list-map li', function(event){
    var lojaId = $(this).prop('id').replace('loja_', '');
    for (var i = 0; i < markers.length; i++) {
        if(markers[i].lojaId == lojaId){
            if(event.type == "mouseover"){
                markers[i].setIcon(icon2);
            }else{
                markers[i].setIcon(icon1);
            }
        }
    }
});

function showInfo() {
    $('.col-list-map ul').html('');
    //console.log(markers);
    for (var i = 0; i < markers.length; i++) {
        //console.log(markers[i].horario_especial.length);
        var currentBounds = map.getBounds()
        var latlng = new google.maps.LatLng(markers[i].position.lat(), markers[i].position.lng());
        if(currentBounds.contains(latlng)){

            var item =   '<li id="loja_'+markers[i].lojaId+'">'
                        +'    <div class="panel panel-default">'
                        +'        <div class="panel-body">'
                        +'            <h4 class="loja">'+markers[i].loja+'</h4>'
                        +'            <p class="endereco">'+markers[i].endereco+', '+markers[i].num+' '+markers[i].comp+'</p>'
                        +'            <p>'
                        +'                <span class="telefone">'+markers[i].telefone+'</span>'
                        +'            </p>'
                        +'            <p class="horario">';
                        if(markers[i].horario_especial.length > 0){
                           
                           item += '<span>'; 
                            $.each(markers[i].horario, function(j,v){
                              
                                item += v.replace("Sab a Sab", "Sab").replace("Dom a Dom", "Dom")+'<br/>';
                            });
                            /* item += '<strong>Horário Especial</strong><br/>';
                            $.each(markers[i].horario_especial, function(y,z){
                               
                                item += z.replace("Sab a Sab", "Sab").replace("Dom a Dom", "Dom")+'<br/>';
                            });*/
                            item +='</span>';
                        }else{
                            item += '<span>'; 
                            $.each(markers[i].horario, function(j,v){
                               
                                item +=  v.replace("Sab a Sab", "Sab").replace("Dom a Dom", "Dom")+"</br>";
                            });
                            item +='</span>';
                          
                        }
                        item += '            </p>'         
                        +'        </div>'
                        +'    </div>'
                        +'</li>';
            $('.col-list-map ul').append(item);

            var ul = $('.col-list-map ul').offset().top;
            $('.col-list-map ul li').each(function(){
                $(this).attr('data-top', $(this).offset().top - ul);
            });

        }
    }
}
function isLocationFree(search) {
    for (var i = 0, l = lookup.length; i < l; i++) {
        if (lookup[i][0] === search[0] && lookup[i][1] === search[1]) {
            return false;
        }
    }
    return true;
}

</script>
<?php get_footer(); ?>