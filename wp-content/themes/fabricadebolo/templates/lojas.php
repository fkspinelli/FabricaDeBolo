<?php
/*
* Template Name: Lojas
*/
get_header();  ?>
 <section class="lojas">
        <div class="search">
            <div class="container">
                <form action="<?php bloginfo('url'); ?>/onde-encontrar" method="get" class="form-inline form-map-search text-center form-search" role="form">
                    <label>Procure a loja mais próxima:</label>
                    <div class="input-group">
                        <input type="text" id="place_input_out" name="place" class="form-control" placeholder="Digite o endereço">

                        <span class="input-group-btn">
                        <button class="btn btn-buscar btn-buscar-home transition" type="submit">Buscar</button>
                    </span>
                    </div>
                </form>
            </div>
        </div>
        <iframe id="iframe1" src="http://fabricadebolo.agenciadizain.com.br/services/lojas/public/" scrolling="no" style="width: 100%; height: 2000px;" frameborder="0"></iframe>
        <script>
            setInterval(function(){
                $('#iframe1').height( $('#iframe1').contents().find("body").height() );
            },500);

            // $('#iframe1').contents().find("body .pagination li a").on(function(){
            //     alert();
            // });

            $('#iframe1').load(function(){

                    var iframe = $('#iframe1').contents();

                    iframe.find("a").click(function(){
                           $('body').scrollTop(0);
                    });
            });

        </script>
    </section>
<?php get_footer(); ?>
