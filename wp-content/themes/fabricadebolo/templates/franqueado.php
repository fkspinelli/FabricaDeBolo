<?php 
/*
* Template Name: Franqueado
*/
get_header('franqueado'); the_post(); ?>
    <section id="seja-um-franqueado" class="banner banner-franqueado" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/banner-franqueado.png);" data-stellar-background-ratio="0.3">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" data-scroll-reveal="enter bottom and move 20px over 1s">
                    <h2><?php the_title(); ?></h2>
                    <h3>Ser um franqueado Vó Alzira é um sonho possível.</h3>
                    <div class="itens">
                        <div class="row">
                            <div class="col-sm-2 col-sm-push-2 col-xs-6"><div class="item"><i class="fa fa-shopping-basket" aria-hidden="true"></i></div><div><span>mais de 200 lojas</span></div></div>
                            <div class="col-sm-2 col-sm-push-2 col-xs-6"><div class="item"><i class="fa fa-money" aria-hidden="true"></i></div><div><span>Não cobramos Royalties</span></div></div>
                            <div class="col-sm-2 col-sm-push-2 col-xs-6"><div class="item"><i class="fa fa-tags" aria-hidden="true"></i></div><div><span>BOLO NÃO É MODA</span></div></div>
                            <div class="col-sm-2 col-sm-push-2 col-xs-6"><div class="item"><i class="fa fa-newspaper-o" aria-hidden="true"></i></div><div><span>Destaque na mídia</span></div></div>
                        </div>
                    </div>
<!--                     <div class="row">
                        <div class="col-sm-8 col-sm-push-2">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <iframe src="https://www.youtube.com/embed/e3QLEFgPI_A" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-3">
                            <a href="#" class="btn btn-danger btn-block text-uppercase">Quero ser um franqueado</a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <section id="nossa-historia" class="franqueado-nossa-historia">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 data-scroll-reveal="enter bottom and move 20px over 1s">Nossa História</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5" data-scroll-reveal="enter left and move 20px over 1s">
                    <img src="<?php bloginfo('template_url'); ?>/images/fotos/vo-alzira.png" class="img-responsive foto">
                    <h6><?php echo get_field('nossa_historia_sm'); ?></h6>
                </div>
                <div class="col-sm-7" data-scroll-reveal="enter right and move 20px over 1s">
                   <?php echo get_field('nossa_historia_lg'); ?>
                    
                    <div class="numero-de-lojas">
                        <div class="content">
                            <span>Mais de</span>
                            <span><?php echo get_field('nossa_historia_lojas'); ?></span>
                            <span>lojas</span>
                        </div>
                    </div>
                    <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/icon/luva.png" class="luva">
                    <div class="numero-de-lojas">
                        <div class="content">
                            <span>Mais de</span>
                            <span><?php echo get_field('nossa_historia_sabores'); ?></span>
                            <span>Sabores</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="depoimentos" style="background-image: url(<?php bloginfo('template_url'); ?>/images/background/background-depoimento.png)" data-stellar-background-ratio="0.1">
        <div class="container">
            
            <div class="row row-depoimentos">
                <?php $cont = 0; $args = array('post_type' => 'testemunhos'); query_posts($args); while(have_posts()): $cont++; the_post(); ?>  
                <div class="col-sm-6">
                    <div class="depo">
                        <div>

                            <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'normal', false); ?>
                        </div>
                        <div>
                            <p><i><?php the_content(); ?></i></p>
                            <h5><?php the_title(); ?></h5>
                            <h6><?php echo get_field('nome_franquia'); ?></h6>
                        </div>
                    </div>
                </div>
                <?php if($cont % 2 == '0'): echo '</div><div class="row">'; endif; endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </section>

    <section id="vantagens" class="franqueado-vantagens" style="background-image: url(<?php bloginfo('template_url'); ?>/images/background/vantagens.png)" data-stellar-background-ratio="0.8">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 data-scroll-reveal="enter bottom and move 20px over 1s">Vantagens</h2>
                </div>
            </div>
            <div class="row" data-scroll-reveal="enter right and move 50px over 1s">
                <div class="col-sm-3">
                    <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/fotos/vo-alzira-2.png" class="foto">
                </div>
                <div class="col-sm-9">
                    
                    <?php echo get_field('vantagem'); ?>
                </div>
            </div>
            <div style="margin-bottom: 40px;"></div>
            <div class="row">
                <div class="col-sm-12">
                    <h4 data-scroll-reveal="enter bottom and move 20px over 1s">Negócio de bolo caseiro é secular. Não é moda.</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                    <div class="row" data-scroll-reveal="enter bottom and move 20px over 1s">
                        <div class="box-vantagens">
                            <i class="fa fa-money" aria-hidden="true"></i>
                            <p>Não cobramos royalties</p>
                        </div>
                        <div class="box-vantagens">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            <p>Assesoria de imprensa</p>
                        </div>
                        <div class="box-vantagens">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <p>treinamento constante</p>
                        </div>
                        <div class="box-vantagens">
                            <i class="fa fa-line-chart" aria-hidden="true"></i>
                            <p>Campanhas de marketing</p>
                        </div>
                        <div class="box-vantagens">
                            <i class="fa fa-glass" aria-hidden="true"></i>
                            <p>Assessoria na inauguração</p>
                        </div>
                        <div class="box-vantagens">
                            <i class="fa fa-cubes" aria-hidden="true"></i>
                            <p>transferência de Know How</p>
                        </div>
                        <div class="box-vantagens">
                            <i class="fa fa-volume-control-phone" aria-hidden="true"></i>
                            <p>Suporte técnico dedicado</p>
                        </div>
                        <div class="box-vantagens">
                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                            <p>acompanhamento de mercado</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">  
                    <div class="row">
                        <div class="col-md-12 col-sm-6" align="center">
                            <div class="tag-circle">
                                <div class="top">
                                    <div>
                                        <span><?php echo get_field('investimento'); ?></span>
                                        <span>mil reais</span>
                                        <span>aproximadamente</span>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <div>
                                        <span>investimento inicial</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6" align="center">
                            <div class="tag-circle">
                                <div class="top">
                                    <div>
                                        <span><?php echo get_field('meses'); ?></span>
                                        <span>meses</span>
                                        <span>em média</span>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <div>
                                        <span>retorno do investimento</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="investimento" class="franqueado-investimento" style="background-image: url(<?php bloginfo('template_url'); ?>/images/background/investimento.png)" data-stellar-background-ratio="0.1">
        <div class="container">
            <div class="row" data-scroll-reveal="enter bottom and move 20px over 1s">
                <div class="col-sm-12">
                    <h2>Investimento</h2>
                    <h6><?php echo get_field('investimento_texto'); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="box-investimento" data-scroll-reveal="enter bottom and move 20px over 1s wait 0s">
                            <i class="fa fa-expand" aria-hidden="true"></i>
                            <p>Instalações de 45m²</p>
                        </div>
                        <div class="box-investimento" data-scroll-reveal="enter bottom and move 20px over 1s wait 0.3s">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <p>Preferencialmente em lojas de rua</p>
                        </div>
                        <div class="box-investimento" data-scroll-reveal="enter bottom and move 20px over 1s wait 0.6s">
                            <i class="fa fa-male" aria-hidden="true"></i>
                            <p>Locais de tráfego intenso de pessoas</p>
                        </div>
                        <div class="box-investimento" data-scroll-reveal="enter bottom and move 20px over 1s wait 0.9s">
                            <i class="fa fa-car" aria-hidden="true"></i>
                            <p>Estacionamento de fácil acesso</p>
                    </div>
                    </div>
                </div>
            </div>
            <div class="row" data-scroll-reveal="enter bottom and move 20px over 1s">
                <div class="col-sm-12">
                    <h3>NÃO COBRAMOS ROYALTIES</h3>
                </div>
            </div>
<!--             <div class="row" data-scroll-reveal="enter bottom and move 20px over 1s">
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="tag-circle">
                        <div class="top">
                            <div>
                                <span><?php echo get_field('taxa'); ?></span>
                                <span>mil reais</span>
                            </div>
                        </div>
                        <div class="bottom">
                            <div>
                                <span>Taxa de Franquia</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="tag-circle">
                        <div class="top">
                            <div>
                                <span><?php echo get_field('equipamento'); ?></span>
                                <span>mil reais</span>
                            </div>
                        </div>
                        <div class="bottom">
                            <div>
                                <span>Equipamentos e instalações</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="tag-circle">
                        <div class="top">
                            <div>
                                <span><?php echo get_field('arquitetura'); ?></span>
                                <span>mil reais</span>
                            </div>
                        </div>
                        <div class="bottom">
                            <div>
                                <span>Projeto de arquitetura</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="tag-circle">
                        <div class="top">
                            <div>
                                <span><?php echo get_field('estoque'); ?></span>
                                <span>mil reais</span>
                            </div>
                        </div>
                        <div class="bottom">
                            <div>
                                <span>Estoque inicial</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3 col-md-push-3">
                    <div class="tag-circle">
                        <div class="top">
                            <div>
                                <span><?php echo get_field('capital'); ?></span>
                                <span>mil reais</span>
                            </div>
                        </div>
                        <div class="bottom">
                            <div>
                                <span>Capital de giro</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3 col-md-push-3">
                    <div class="tag-circle">
                        <div class="top">
                            <div>
                                <span><?php echo get_field('publicidade'); ?></span>
                                <span>reais / mês</span>
                            </div>
                        </div>
                        <div class="bottom">
                            <div>
                                <span>Taxa de Publicidade</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
<!--             <div class="row">
                <div class="col-sm-6 col-sm-push-3">
                    <a href="#" class="btn btn-danger btn-block text-uppercase">Quero ser um franqueado</a>
                </div>
            </div> -->
        </div>
    </section>
<!-- 
    <section id="area-de-interesse" class="franqueado-area-de-interesse" style="background-image: url(<?php echo bloginfo('template_url') ?>/images/banner/banner-area-de-interesse.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Área de Interesse</h2>
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <p>Ficha de pré-qualificação para candidatos à franquia Fábrica de Bolos. Após o preenchimento enviar a ficha para: </p>
                        <p><b>franquia@fabricadebolo.com</b></p>
                    </div>
                    <div class="col-sm-4">
                        <a href="<?php echo bloginfo('template_url'); ?>/xls/cadastro.xls" class="btn btn-danger btn-block text-uppercase" download>baixar</a>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <section id="cadastre-se" class="quero-ser-um-franqueado">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p>
                        Cadastre-se agora e venha fazer parte da franquia mais saborosa do momento.</p>
                        <p style="list-style: none; font-size: 16px; font-weight: 400; color: #000000; line-height: 26px; ">Clique no botão, preencha o formulário e envie para franquia@fabricadebolo.com</p> </div>
                <div class="col-sm-6">
                    <!-- <a href="<?php echo get_field('arquivo_excel'); ?>" class="btn btn-danger btn-block text-uppercase" download>Quero ser um franqueado</a> -->
                    <a href="<?php echo get_field('arquivo_excel'); ?>" class="btn btn-danger btn-block text-uppercase" download>Quero ser um franqueado</a>
                </div>
            </div>
        </div>
    </section>

    <section class="obs">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Observações</h4>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
    $(document).ready(function(){
        if($(window).width() > 990){
           $.stellar({
                horizontalScrolling: false
            }); 
        }
    });
    </script>
<?php get_footer(); ?>