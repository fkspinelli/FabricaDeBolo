<?php 
/*
* Template Name: Novidades
*/
get_header(); 
?>
    
    <?php query_posts(['post_type'=>'post','cat' => '7']); while(have_posts()): the_post();
        $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'large');
     ?>
    <section class="banner novidades" style="background-image: url(<?php echo $img[0]; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2><?php the_title(); ?></h2>
                    <?php //the_content(); ?>
                </div>
            </div>
        </div>
    </section>
    <?php endwhile; wp_reset_query(); ?>
    <section class="form-box">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-push-3">
                    <div class="form-group">
                        <select name="r" class="selectpicker form-control">
                            <option selected disabled>Ordenar por</option>
                            <option value="1">De A - Z</option>
                            <option value="2">De Z a A</option>
                            <option value="3">Pelo mais antigo</option>
                            <option value="4">Pelo mais recente</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="news">
            <div class="container">

                <div class="row">
                    <?php $cont = 0; query_posts(['post_type'=>'post','category__not_in' => array(7,11)]); while(have_posts()): the_post(); ?>
                    <div class="col-sm-6">

                        <a href="<?php echo the_permalink(); ?>" class="box-info">

                            <div class="row">
                                <div class="col-sm-6">
                                    <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'thumb', false, ['class'=>'img-responsive pull-left']); ?>
                                </div>
                                <div class="col-sm-6 box-text">
                                    <div class="text">
                                        <p><?php the_date(); ?></p>
                                        <p><?php the_title(); ?></p>
                                        <p><?php the_excerpt(); ?></p>
                                    </div>
                                </div>
                            </div>

                        </a>
                    </div>
                    <?php 
                        $cont++; if($cont%2 == '0'): echo '</div><div class="row">'; endif; 
                        endwhile; wp_reset_query();
                    ?>
                </div>

                <!--div class="row">
                    <div class="col-sm-6 col-sm-push-3">
                        <a href="#" class="btn btn-warning btn-block btn-lg">Ver mais</a>
                    </div>
                </div-->
            </div>
        </div>
    </section>
 <?php get_footer(); ?>