<?php get_header(); the_post();   ?>
<section>
  <?php query_posts(['post_type'=>'post','cat' => '7']); while(have_posts()): the_post();
        $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'large');
     ?>
    <section class="banner novidades" style="background-image: url(<?php echo $img[0]; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2><?php the_title(); ?></h2>
                    <?php //the_content(); ?>
                </div>
            </div>
        </div>
    </section>
    <?php endwhile; wp_reset_query(); ?>
<section>
    <div class="news interna">
        <div class="container" style="position: relative;">
            <div class="row">
                <div class="col-sm-12">
                	<a href="<?php bloginfo('url'); ?>/novidades" class="back"><img src="<?php echo bloginfo('template_url'); ?>/images/icon/back.png" class="img-responsive"> Voltar para lista</a>
                    <div class="box-info">
                        <div class="row">
                            <div class="col-sm-6">
                            <?php $img = get_field('arquivo'); //echo wp_get_attachment_image(get_post_thumbnail_id(), 'large', false, ['class'=>'img-responsive']); ?>
                                <img src="<?php echo $img['sizes']['medium_large']; ?>" class="img-responsive" alt="">
                            </div>
                            <div class="col-sm-6 box-text">
                                <div class="text">
                                    <p><?php the_date(); ?></p>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $next = get_next_post( true ); $prev = get_previous_post( true );  ?>
            <?php if (!empty( $prev )): ?>
                <a href="<?php echo get_the_permalink($prev->ID); ?>" class="prev"><div></div></a>
            <?php endif; ?>
            <?php if (!empty( $next )): ?>
                <a href="<?php echo get_the_permalink($next->ID); ?>" class="next"><div></div></a>
            <?php endif; ?>
        </div>
    </div>
</section>

<section>
    <div class="news">
        <div class="container">
            <img src="<?php echo bloginfo('template_url'); ?>/images/tag/outras-novidades.png" class="img-responsive tag">
            <div class="row">

            <?php $args = array('post_type'=>'post', 'posts_per_page' => '4', 'post__not_in'=>array($post->ID), 'category__not_in' => array(7,11)); query_posts($args); while (have_posts()): the_post();
             ?>
                <div class="col-sm-6">
                    <div class="box-info">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'medium', false, ['class'=>'img-responsive pull-left']); ?>
                            </div>
                            <div class="col-sm-6 box-text">
                                <div class="text">
                                    <p><?php the_date(); ?></p>
                                    <p><?php the_title(); ?></p>
                                    <p><?php the_excerpt(); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; wp_reset_query(); ?>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <a href="<?php bloginfo('url'); ?>/novidades" class="btn btn-danger btn-block btn-lg text-uppercase">Ver todas</a>
                </div>
            </div>
        </div>
    </div>
</section>
	
<?php get_footer(); ?>