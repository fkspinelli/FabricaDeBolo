<?php

class LojaController extends BaseController {
	protected $layout = 'layout.app';

	public function index()
	{
		$l = Loja::orderBy('id','desc');
		if(Input::has('s')){
			$l->where('nome', 'like', '%'.Input::get('s').'%')
			->whereOr('bairro', 'like', '%'.Input::get('s').'%')
			->whereOr('cidade', 'like', '%'.Input::get('s').'%');
		}
		$lojas = $l->paginate(25);

		$this->layout->content = View::make('loja.index', compact('lojas'));
	}
	public function create()
	{
		$this->layout->content = View::make('loja.index', compact('lojas'));
		
	}
	public function store()
	{
		$data = Input::all();

		$loja = Loja::create($data);

		$h = explode(';', $data['horarios']);
		$h_s = explode(';', $data['horario_especial']);
		$loja->horarios = serialize($h);		
		$loja->horario_especial = serialize($h_s);

		$loja->save();

		return Redirect::route('home')->withSuccess('Loja criada com sucesso');
	}
	public function edit($id)
	{
		$loja = Loja::find($id);

		$this->layout->content = View::make('loja.edit', compact('loja'));
	}

	public function update($id)
	{
		$data = Input::all();
		$loja = Loja::find($id);
		
		// Aqui eu modifico os dados de geolocalizão
		if($data['endereco'] != $loja->endereco || $data['bairro'] != $loja->bairro || $data['cidade'] != $loja->cidade || $data['uf'] != $loja->uf)
		{

		}		

		$loja->update($data);
		
		$h = explode(';', $data['horarios']);
		$h_s = explode(';', $data['horario_especial']);
		$loja->horarios = serialize($h);		
		$loja->horario_especial = serialize($h_s);
		
		$loja->save();

		return Redirect::route('loja.edit', $loja->id)->withSuccess('Loja editada com sucesso');

	}

	public function destroy($id)
	{
		$loja = Loja::find($id);

		$loja->delete();
	}
}
