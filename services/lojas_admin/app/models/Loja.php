<?php 
/**
* Loa
*/
class Loja extends \Eloquent
{
	protected $table = 'tb_lojas';
	protected $fillable = ['nome','razao_social','cnpj','data_cadastro','data_fundacao','data_atualizacao','cep','email','endereco','numero','complemento','bairro','cidade','uf','tel','updatable'];

	public function getHorarios($s='false')
	{
		
		if($s == 'false'){ 

			$h = is_array(unserialize($this->horarios)) ? $this->horarios : serialize(array());
		}else{
			$h = is_array(unserialize($this->horario_especial)) ? $this->horario_especial : serialize(array());
		}
		return implode('; ', unserialize($h));
	}   
}