<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Fábrica de Bolo - Admin Lojas</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
</head>
<body>
<header>
	
</header>
<section>	
	<div class="container">
		@yield('content')	
	</div>
</section>
<footer></footer>
</body>
</html>