@section('content')
<h1>Editar <small>({{ $loja->nome }})</small></h1>
<a class="link pull-right" href="{{ route('home') }}">Voltar</a>
{{ Form::open(['route'=>array('loja.update',$loja->id), 'method'=>'put', 'class'=>'form']) }}
	<div class="form-group">
		<label for="nome">Nome</label>
		{{ Form::text('nome', $loja->nome, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="razao_social">razao_social</label>
		{{ Form::text('razao_social', $loja->razao_social, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="cnpj">cnpj</label>
		{{ Form::text('cnpj', $loja->cnpj, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="cep">cep</label>
		{{ Form::text('cep', $loja->cep, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="email">email</label>
		{{ Form::text('email', $loja->email, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="endereco">endereco</label>
		{{ Form::text('endereco', $loja->endereco, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="numero">numero</label>
		{{ Form::text('numero', $loja->numero, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="complemento">complemento</label>
		{{ Form::text('complemento', $loja->complemento, ['class'=>'form-control']) }}
	</div>	
	<div class="form-group">
		<label for="bairro">bairro</label>
		{{ Form::text('bairro', $loja->bairro, ['class'=>'form-control']) }}
	</div>	
	<div class="form-group">
		<label for="cidade">cidade</label>
		{{ Form::text('cidade', $loja->cidade, ['class'=>'form-control']) }}
	</div>	
	<div class="form-group">
		<label for="uf">uf</label>
		{{ Form::text('uf', $loja->uf, ['class'=>'form-control']) }}
	</div>	
	<div class="form-group">
		<label for="horarios">horarios</label>
		
		{{ Form::textarea('horarios', $loja->getHorarios(), ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="horarios">horarios especiais</label>
		
		{{ Form::textarea('horario_especial', $loja->getHorarios('true'), ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="uf">uf</label>
		{{ Form::text('uf', $loja->uf, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		<label for="updatable">Editável pelo Franqueado?</label>
		<p>{{ Form::radio('updatable', '0', $loja->updatable == '0'? true : false) }} Sim</p>
		<p>{{ Form::radio('updatable', '1', $loja->updatable == '1'? true : false) }} Não</p>
	</div>	
	<div class="form-group text-right">
		{{ Form::button('Salvar', ['type'=>'submit', 'class'=>'btn btn-primary']) }}
	</div>
{{ Form::close() }}
@stop