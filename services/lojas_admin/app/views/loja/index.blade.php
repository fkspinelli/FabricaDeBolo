@section('content')
<dir class="row">
	<div class="col-md-12 text-right">
		{{ Form::open(['route'=>'home', 'method'=>'get', 'class'=>'form-inline form']) }}
		{{ Form::text('s', Input::get('s'), ['class'=>'form-control', 'placeholder'=>'Buscar por']) }}
		{{ Form::button('<i class="fa fa-search"></i>', ['type'=>'submit', 'class'=>'btn btn-primary']) }}
		{{ Form::close() }}
	</div>
</dir>
<table class="table table-striped table-bordered">
	<tr>
		<th>Nome</th>
		<th>Bairro</th>
		<th>Cidade</th>
		<th>UF</th>
		<th>Editar</th>
	</tr>
	@foreach($lojas as $loja)
	<tr>
		<td>{{ $loja->nome }}</td>
		<td>{{ $loja->bairro }}</td>
		<td>{{ $loja->cidade }}</td>
		<td>{{ $loja->uf }}</td>
		
		<td><a href="{{ route('loja.edit', $loja->id) }}"><i class="fa fa-edit"></i></a></td>		
	</tr>
	@endforeach
</table>
<div class="row">
	<div class="col-md-12 text-center">
		{{ $lojas->appends($_GET)->links() }}	
	</div>
</div>
@stop