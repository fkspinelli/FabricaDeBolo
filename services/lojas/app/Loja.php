<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loja extends Model
{
    protected $table = 'tb_lojas';

    public function getHorarios($special = false)
    {	
    	$h = $special == false ? $this->horarios : $this->horario_especial;
    	if($h == ''){
    		return null;
    	}

    	$arr = unserialize($h);
        $imploded = implode("</p><p>", $arr);
        
        $rep = str_replace(array('Sab a Sab', 'Dom a Dom'), array('Sab', 'Dom'), $imploded);
        
    	$res =  "<div class='horarios2'><p>" . $rep . "</p></div>";

    	echo $res;
    }
    public function formatted_address()
    {
    	return  $this->endereco.', '.$this->numero.' '.$this->complemento.' '.$this->bairro.', '.$this->cidade.' - '.$this->uf;
    }
}
