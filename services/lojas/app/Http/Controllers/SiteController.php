<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Loja;

class SiteController extends Controller
{
    public function home()
    {
    	$lojas = Loja::orderBy('nome','ASC')->paginate(400);
    	$estados = Loja::groupBy('uf')->orderBy('uf', 'asc')->lists('uf','uf');
    	//print_r($estados);
    	return view('site.homepage', compact('lojas','estados'));
    }
    public function searchCities(Request $request)
    {
    	$data = $request->all();

    	$cidades = Loja::where('uf', $data['valor'])->groupBy('cidade')->orderBy('cidade')->lists('cidade','cidade');

    	return response()->json($cidades);
    }
    public function searchBairros(Request $request)
    {
        $data = $request->all();

        $bairros = Loja::where('cidade', $data['valor'])->groupBy('bairro')->orderBy('bairro')->lists('bairro','bairro');

        return response()->json($bairros);
    }
}
