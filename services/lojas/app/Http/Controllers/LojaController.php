<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Loja;

class LojaController extends Controller
{
    public function busca(Request $req)
    {
    	$data = $req->all();

    	$cidades = array();
        $bairros = array();

    	if($data['e'] == '' and $data['c'] == ''){
    		return redirect()->route('home');
    	}

    	$l = Loja::query();

    	if(isset($data['e'])){
    		if($data['e'] != ''){
    			$l->where('uf', $data['e']);
    		}
    		$cidades = Loja::where('uf', $data['e'])->groupBy('cidade')->orderBy('cidade')->lists('cidade','cidade');
    	}
    	if(isset($data['c'])){
    		if($data['c'] != ''){
    			$l->where('cidade', strtolower($data['c']));
    		}
            $bairros = Loja::where('cidade', $data['c'])->groupBy('bairro')->orderBy('bairro')->lists('bairro','bairro');
    	}
        if(isset($data['b'])){
            if($data['b'] != ''){
                $l->where('bairro', strtolower($data['b']));
            }
        }
    	$estados = Loja::groupBy('uf')->orderBy('uf', 'asc')->lists('uf','uf');
    	
        $lojas = $l->orderBy('nome')->paginate(400);
    	
    	return view('site.homepage', compact('lojas','estados','cidades','bairros'));
    }

}
