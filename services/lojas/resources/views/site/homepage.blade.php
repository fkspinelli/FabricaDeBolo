@extends('layout.register')
@section('content')
<section id="lojas" style="margin-top: 0;" class="lojas">
	<div class="container">
        <div class="row filter">
                <div class="col-md-12 text-center">
                    <form action="/services/lojas/public/busca/lojas" class="form-inline" method="get">
                        <div class="form-group">
                            <select id="estado_combo" name="e" class="form-control">
                                <option value="">Selecione Estado</option>
                                @foreach($estados as $e)
                                @if($e!='')
                                <option {{ Input::get('e') == $e ? 'selected' : null }} value="{{ $e }}">{{ $e }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="cidade_combo" name="c" {{ Input::has('e') ? null : 'disabled' }} class="form-control">
                                <option value="">Selecione Cidade</option>
                                @if(Input::has('e'))
                                    @foreach($cidades as $c)
                                        <option {{ Input::get('c') == $c ? 'selected' : null }} value="{{ $c }}">{{ $c }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="b" id="bairro_combo" {{ Input::has('b') ? null : 'disabled' }}  class="form-control">
                                 <option value="">Selecione Bairro</option>
                                 @if(Input::get('b'))
                                     @foreach($bairros as $b)
                                        <option {{ Input::get('b') == $b ? 'selected' : null }} value="{{ $b }}">{{ $b }}</option>
                                    @endforeach
                                 @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <button style="background: #ee1e26;border: 1px solid #ee1e26;" class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>

{{--                <div class="col-lg-2 col-lg-push-4 col-md-3 col-md-push-3 col-sm-3 col-sm-push-3">
                    <div class="form-group">
                        <select id="estado_combo" name="e" class="form-control">
                        	<option value="">Selecione Estado</option>
                        	@foreach($estados as $e)
							@if($e!='')
							<option {{ Input::get('e') == $e ? 'selected' : null }} value="{{ $e }}">{{ $e }}</option>
							@endif
                        	@endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-push-4 col-md-3 col-md-push-3 col-sm-3 col-sm-push-3">
                    <div class="form-group">
                      <select id="cidade_combo" name="c" {{ Input::has('e') ? null : 'disabled' }} class="form-control">
                        	<option value="">Selecione Cidade</option>
                        	@if(Input::has('e'))
	                        	@foreach($cidades as $c)
									<option {{ Input::get('c') == $c ? 'selected' : null }} value="{{ $c }}">{{ $c }}</option>
	                        	@endforeach
                        	@endif
                        	</select>
                       <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div> --}}

        </div>
        {{--<div class="row">
            <div class="col-sm-12">
                <h2 class="loja_title">

                	@if(Input::has('c'))
					{{  ucwords(strtolower(Input::get('c'))) }}
					@elseif(Input::has('e'))
					{{ Input::get('e') }}
                	@else
                	Todas as Lojas
                	@endif
                </h2>
            </div>
        </div>--}}
{{--         <div class="row">
            <div class="col-md-12">
                <ul>
                	@foreach($lojas as $loja)
                    <li>
                        <a href="#">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4 class="loja">{{ $loja->nome }}</h4>
                                    <p>
                                        <span class="telefone">{{ $loja->tel }}</span>
                                        <span class="horario">
                                        {{ $loja->getHorarios() }}

                                        @if($loja->horario_especial != 'a:0:{}')
                                        <br/>
                                        <strong>Horários especiais</strong><br/>
										{{ $loja->getHorarios(true) }}
										@endif
                                        </span>

                                    </p>
                                    <p class="endereco">{{ $loja->formatted_address() }}</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>

        </div> --}}
        @foreach($lojas->chunk(2) as $chunk)
            <div class="row">
                @foreach($chunk as $loja)
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4 class="loja">{{ $loja->nome }}</h4>
                                <p>
                                    <span class="telefone">{{ $loja->tel }}</span>
                                </p>
                                {{-- <p class="horario"> --}}
                                {{ $loja->getHorarios() }}

                               {{-- @if($loja->horario_especial != 'a:0:{}')
                                <br/>
                                <strong>Horários especiais</strong><br/>
                                {{ $loja->getHorarios(true) }}
                                @endif --}}
                                {{-- </p> --}}
                                <p class="endereco">{{ $loja->formatted_address() }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach

        <div class="row">
            <div class="col-md-12 text-center">
                {{ $lojas->appends($_GET)->links() }}
            </div>
        </div>
    </div>
</section>
 <script>
	 $.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
 	$('#estado_combo').change(function(){
 		var valor = $(this).val();
 		$('#cidade_combo').empty().append('<option value="">Carregando...</option>');
        $('#bairro_combo').empty().append('<option value="">Selecione Bairro</option>').prop('disabled',true);
 		$.ajax({
 			url: '/services/lojas/public/busca/cidades',
 			type: 'post',
 			data: {valor:valor}
 		}).done(function(data){
 			console.log(data);
 			res = "";
 			$.each(data, function(i,v){
 				res += '<option value="'+v+'">'+v+'</option>';
 			});
 			$('#cidade_combo').empty();
 			$('#cidade_combo').append('<option value="">Selecione Cidade</option>');
 			$('#cidade_combo').append(res).prop('disabled',false);

 		});
 	});

    $('#cidade_combo').change(function(){
        var valor = $(this).val();
        $('#bairro_combo').empty().append('<option value="">Carregando...</option>');
        $.ajax({
            url: '/services/lojas/public/busca/bairros',
            type: 'post',
            data: {valor:valor}
        }).done(function(data){
            console.log(data);
            res = "";
            $.each(data, function(i,v){
                res += '<option value="'+v+'">'+v+'</option>';
            });
            $('#bairro_combo').empty();
            $('#bairro_combo').append('<option value="">Selecione Bairro</option>');
            $('#bairro_combo').append(res).prop('disabled',false);

        });
    });

    var maxHeight = 0;
    $('#lojas .panel-body').each(function(){
        if($(this).height() > maxHeight){
            maxHeight = $(this).height();
        }
    });
    $('#lojas .panel-body').css({'min-height':maxHeight});

 </script>
<script>
    var iFrameResizer = {
            messageCallback: function(message){
                alert(message,parentIFrame.getId());
            }
        }
</script>
 <style type="text/css">
.lojas ul {
    margin-left: 30px !important;
    text-indent: 0;
    margin-top: -7px !important;
}

.lojas strong {
    margin-bottom: 30px;
    display: inline-block;
}
.horarios2:before {
    display: block;
    content: "";
    background: url(http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/images/icon/calendar.png);
    width: 23px;
    height: 21px;
    float: left;
}
.horarios2 p {
    padding-left: 30px;
    line-height: 10px;
}
@media (max-width: 784px){
button.btn.btn-primary {
    width: 100%;
}
}
</style>
@endsection
