<html>
	<head>
		<title>Sistema busca Lojas</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
		 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="icon" href="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/favicon.ico" type="image/gif" sizes="16x16">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.1.1/css/bootstrap-slider.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/css/beauty-font.css">
    <link rel="stylesheet" type="text/css" href="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/css/responsive.css">
    <script src="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/js/jquery.min.js"></script>
    <script src="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/js/iframeResizer.contentWindow.min.js"></script>
    <script src="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/js/bootstrap.min.js"></script>
    <script src="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/js/bootstrap-slider.min.js"></script>
    <script src="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/js/jquery.bxslider.min.js"></script>

    <script src="https://ihatetomatoes.net/demos/scrolling-libraries/js/scrollReveal.js"></script>
    <script src="http://markdalgleish.com/projects/stellar.js/js/stellar.js"></script>
    <script src="http://fabricadebolo.agenciadizain.com.br/wp-content/themes/fabricadebolo/js/script.js"></script>
	</head>
	<body>
		@yield('content')
	</body>
</html>
